## Adminpannel Project

This application was generated using HTML + CSS + JS + JSP 

## Feature List

1- Multi-Language support ( switch between English and Farsi page dynamically)
2- Generate Table with JSP and jstl tag
3- Using static Data Model and DAO layer for simulating back-end 
3- Ability to search , delete and edit for each rows of table
4- using fancy detail box for editing contact list 

## Development

This application is dynamic web project and recommend to deploy on Apache Tomcat 8.5 and 
JDK 8

## Access URL

If you start your tomcat on port 8080 ( default port) then you can access the En and Fa pages as below URL :

English page : http://localhost:8080/adminpannel/starter-En-details.jsp
Farsi page : http://localhost:8080/adminpannel/starter-Fa-details.jsp


