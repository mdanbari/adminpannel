package ir.ariaban.demo;

public class Contact {

	private String id;
	private String number;
	private String shortNumber;
	private String numberType;
	private String terminalType;
	private String status;
	private String subscriber;

	public Contact() {
		// TODO Auto-generated constructor stub
	}

	public Contact(String id, String number, String shortNumber, String numberType, String terminalType, String status,
			String subscriber) {
		super();
		this.id = id;
		this.number = number;
		this.shortNumber = shortNumber;
		this.numberType = numberType;
		this.terminalType = terminalType;
		this.status = status;
		this.subscriber = subscriber;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getShortNumber() {
		return shortNumber;
	}

	public void setShortNumber(String shortNumber) {
		this.shortNumber = shortNumber;
	}

	public String getNumberType() {
		return numberType;
	}

	public void setNumberType(String numberType) {
		this.numberType = numberType;
	}

	public String getTerminalType() {
		return terminalType;
	}

	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}

}
