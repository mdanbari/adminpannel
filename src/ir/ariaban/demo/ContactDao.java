package ir.ariaban.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class ContactDao {

	private static HashMap<String, Contact> contactList = new HashMap<>();

	static {

		contactList = initialData();

	}

	public static Contact editContact;
	
	public static HashMap<String, Contact> initialData()
	{
		HashMap<String, Contact> initailList = new HashMap<>();
		Contact contact1 = new Contact("1", "123456789", "123", "DID", "Access", "Approved", "Bacon ipsum dolor");
		Contact contact2 = new Contact("2", "56465465", "564", "DID", "Access", "Pending", "Bacon ipsum dolor");
		Contact contact3 = new Contact("3", "654654564", "654", "DID", "Access", "Normal", "Bacon ipsum dolor");
		Contact contact4 = new Contact("4", "987654554", "987", "DID", "Access", "Denied", "Bacon ipsum dolor");
		Contact contact5 = new Contact("5", "965564544", "965", "DID", "Access", "Approved", "Bacon ipsum dolor");
		initailList.put("1", contact1);
		initailList.put("2", contact2);
		initailList.put("3", contact3);
		initailList.put("4", contact4);
		initailList.put("5", contact5);
		return initailList;
	}

	public static List<Contact> getAllContact() {
		return new ArrayList<>(contactList.values());
	}

	public static List<Header> getTableHeader(String locale) {
		List<Header> headers = new ArrayList<>();
		if ("en".equals(locale))
			headers = Arrays.asList(new Header("ID"), new Header("Number"), new Header("Short Number"),
					new Header("Number Type"), new Header("Terminal Type"), new Header("Status"),
					new Header("Subscriber"), new Header("Edit"), new Header("Delete"));
		else
			headers = Arrays.asList(new Header("شناسه"), new Header("شماره"), new Header("خلاصه شماره"),
					new Header("نوع شماره"), new Header("ترمینال"), new Header("وضعیت"), new Header("دریافت کننده"),
					new Header("ویرایش"), new Header("حذف"));
		return headers;
	}

	public static void deleteContact(Contact contact) {
		contactList.remove(contact.getId());
	}
	
	public static void searchContact(String number) {
		
		//String number = contact.getNumber();
		HashMap<String, Contact> searchList = new HashMap<>();
		if(number == null || "".equals(number)) {
			contactList = initialData();
		} else{
			for (Entry<String, Contact> e : contactList.entrySet()) {
				Contact value = e.getValue();
				String key = e.getKey();
				if(value.getNumber().contains(number)){
					searchList.put(key, value);
			    }
			}
			contactList = searchList;
		}
		
	}
	
	public static Contact editById(String id){
		return contactList.get(id);
	}
	
	public static boolean updateContact(Contact contact){
		Contact dbContact = contactList.get(contact.getId());
		
		if(dbContact==null)
			return false;
		else
		{
			contactList.remove(contact.getId());
			contactList.put(contact.getId(), contact);
		}
		return true;
	}

}
