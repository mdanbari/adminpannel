
<%@page import="ir.ariaban.demo.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><!DOCTYPE html>
<%@page import="ir.ariaban.demo.Contact.*,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Admin</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet"
	href="bower_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet"
	href="bower_components/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet"
	href="bower_components/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="dist/css/Admin-En.min.css">
<link rel="stylesheet" href="plugins/iCheck/square/yellow.css">
<link rel="stylesheet" href="dist/css/skins/skin-yellow.min.css">
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

<script>"undefined"==typeof CODE_LIVE&&(!function(e){var t={nonSecure:"50849",secure:"50858"},c={nonSecure:"http://",secure:"https://"},r={nonSecure:"127.0.0.1",secure:"gapdebug.local.genuitec.com"},n="https:"===window.location.protocol?"secure":"nonSecure";script=e.createElement("script"),script.type="text/javascript",script.async=!0,script.src=c[n]+r[n]+":"+t[n]+"/codelive-assets/bundle.js",e.getElementsByTagName("head")[0].appendChild(script)}(document),CODE_LIVE=!0);</script>
</head>
<body class="hold-transition skin-yellow sidebar-mini"
	data-genuitec-lp-enabled="false" data-genuitec-file-id="wc1-62"
	data-genuitec-path="/adminpannel/WebContent/AdminPanel/starter-En-details.jsp">
	<div class="wrapper" data-genuitec-lp-enabled="false"
		data-genuitec-file-id="wc1-62"
		data-genuitec-path="/adminpannel/WebContent/AdminPanel/starter-En-details.jsp">

		<!-- Main Header -->
		<header class="main-header">

			<!-- Logo -->
			<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>A</b>P</span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Admin</b>Panel</span>
			</a>

			<!-- Header Navbar -->
			<nav class="navbar navbar-static-top" role="navigation">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu"
					role="button"> <span class="sr-only">Toggle navigation</span>
				</a>
				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">

						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu">
							<!-- Menu toggle button --> <a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <i class="fa fa-envelope-o"></i> <!--              <span class="label label-success">1</span>-->
						</a>
						</li>

						<!-- Notifications Menu -->
						<li class="dropdown notifications-menu">
							<!-- Menu toggle button --> <a href="#" class="dropdown-toggle"
							data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <!--              <span class="label label-warning">1</span>-->
						</a>
						</li>

						<!-- Tasks Menu -->
						<li class="dropdown tasks-menu">
							<!-- Menu Toggle Button --> <a href="starter-Fa-details.jsp">
								<i class="fa fa-flag-o"></i> <!--              <span class="label label-danger">1</span>-->
						</a>
						</li>

						<!-- User Account Menu -->
						<li class="dropdown user user-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"> <img
								src="dist/img/avatar5.png" class="user-image" alt="User Image">
								<span class="hidden-xs">User Name</span>
						</a></li>

					</ul>
				</div>
			</nav>
		</header>

		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">

			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">

				<!-- search form (Optional) -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control"
							placeholder="Search..."> <span class="input-group-btn">
							<button type="submit" name="search" id="search-btn"
								class="btn btn-flat">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->

				<!-- Sidebar Menu -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="header">Main Menu</li>
					<!-- Optionally, you can add icons to the links -->
					<li class="active"><a href="#"><i class="fa fa-link"></i>
							<span>Page Name</span></a></li>
				</ul>
				<!-- /.sidebar-menu -->
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Page Header <small>Optional description</small>
				</h1>
			</section>

			<!-- Main content -->
			<section class="content container-fluid">



				<c:choose>
					<c:when test="${editContact!=null}">
						<div class="col-md-8">
					</c:when>					
					<c:otherwise>
						<div class="col-md-12">
					</c:otherwise>
				</c:choose>
				

					<div class="row rowBtnPlace">

						<div class="box">
							<div class="box-body">

								<button class="btn btn-warning">Assign Short Num</button>
								<button class="btn btn-default">Import Phone</button>
								<button class="btn btn-default">Configure</button>
								<div class="btn-group">
									<button type="button" class="btn btn-default">Operate
										by Filter</button>
									<button type="button" class="btn btn-default dropdown-toggle"
										data-toggle="dropdown">
										<span class="caret"></span> <span class="sr-only">Toggle
											Dropdown</span>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Action</a></li>
										<li><a href="#">Another action</a></li>
									</ul>
								</div>

							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-xs-12">
							<div class="box">

								<div class="box-header">
									<h3 class="box-title">Number List</h3>
									<div class="box-tools">

										<form action="serachContact.jsp" method="post">
											<div class="input-group" style="width: 270px;">
												<input type="text" name="number"
													class="form-control input-sm pull-right"
													placeholder="Search"> <input type="hidden"
													name="locale" value="en">
												<div class="input-group-btn">
													<button type="submit" class="btn btn-sm btn-warning">
														<i class="fa fa-search"></i>
													</button>
												</div>
											</div>
										</form>

									</div>
								</div>
								<!-- /.box-header -->

								

								<%
									List<Contact> contactList = ContactDao.getAllContact();
									List<Header> headerList = ContactDao.getTableHeader("en");
									request.setAttribute("contactList", contactList);
									request.setAttribute("headerList", headerList);
								%>


								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tr>
											<th><div class="checkbox icheck">
													<label><input type="checkbox"></label>
												</div></th>
											<c:forEach items="${headerList}" var="h">
												<th>${h.getTitle()}</th>
											</c:forEach>
											<th></th>
										</tr>
										<c:forEach items="${contactList}" var="u">
											<tr>
												<td><div class="checkbox icheck">
														<label><input type="checkbox"></label>
													</div></td>
												<td>${u.getId()}</td>
												<td>${u.getNumber()}</td>
												<td>${u.getShortNumber()}</td>
												<td>${u.getNumberType()}</td>
												<td>${u.getTerminalType()}</td>
												<c:choose>
													<c:when test="${u.getStatus() ==  'Approved'}">
														<td><span class="label label-success">${u.getStatus()}</span></td>
													</c:when>
													<c:when test="${u.getStatus() ==  'Pending'}">
														<td><span class="label label-warning">${u.getStatus()}</span></td>
													</c:when>
													<c:when test="${u.getStatus() ==  'Normal'}">
														<td><span class="label label-primary">${u.getStatus()}</span></td>
													</c:when>
													<c:otherwise>
														<td><span class="label label-danger">${u.getStatus()}</span></td>
													</c:otherwise>
												</c:choose>
												<td>${u.getSubscriber()}</td>
												<td><a href="editContact.jsp?id=${u.getId()}&locale=en">
														<span class="label label-warning"> Edit </span>
												</a></td>
												<td><a
													href="deleteContact.jsp?id=${u.getId()}&locale=en"><span
														class="label label-danger">Delete</span> </a></td>
												<td><a href="editContact.jsp?id=${u.getId()}&locale=en"><i
														class="fa fa-angle-right"></i></a></td>
											</tr>
										</c:forEach>


									</table>
								</div>
								<!-- /.box-body -->

								<div class="box-footer clearfix">
									<ul class="pagination pagination-sm no-margin pull-right">
										<li><a href="#">&laquo;</a></li>
										<li><a href="#">1</a></li>										
										<li><a href="#">&raquo;</a></li>
									</ul>
								</div>

							</div>
							<!-- /.box -->
						</div>
					</div>

				</div>

				<c:choose>
					<c:when test="${editContact!=null}">
						<div class="col-md-4">

							<div class="box box-warning">
								<div class="box-header">
									<i class="fa fa-info"></i>
									<h3 class="box-title">Contact Detail</h3>
								</div>
								<div class="box-body">

									<form action="editConfirm.jsp" method="post" class="form-horizontal">
									<input type="hidden" name = "id" value="${editContact.getId()}" >
									<input type="hidden" name = "locale" value="en" >
										<div class="box-body">
											<div class="form-group">
												<label for="text01" class="col-sm-5 control-label">Number</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="text01" name = "number"
														placeholder="1234567890"
														value="${editContact.getNumber()}">
												</div>
											</div>
											<div class="form-group">
												<label for="text02" class="col-sm-5 control-label">Number
													Type</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="text02" name = "numberType"
														placeholder="Type" value="${editContact.getNumberType()}">
												</div>
											</div>
											<div class="form-group">
												<label for="text03" class="col-sm-5 control-label">Short
													Number</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="text03" name = "shortNumber"
														placeholder="1234" value="${editContact.getShortNumber()}">
												</div>
											</div>
											<div class="form-group">
												<label for="text06" class="col-sm-5 control-label">Terminal 
													Type </label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="text06" name = "terminalType"
														placeholder="" value="${editContact.getTerminalType()}">
												</div>
											</div>
											<div class="form-group">
												<label for="text06" class="col-sm-5 control-label">Status
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="text06" name = "status"
														placeholder="" value="${editContact.getStatus()}">
												</div>
											</div>
											<div class="form-group">
												<label for="text06" class="col-sm-5 control-label">Subscriber
												</label>
												<div class="col-sm-7">
													<input type="text" class="form-control" id="text06" name = "subscriber"
														placeholder="" value="${editContact.getSubscriber()}">
												</div>
											</div>

										</div>
										<!-- /.box-body -->
										<div class="box-footer" style="text-align: center;">
											<button type="submit" class="btn btn-warning">Edit</button>
											<a href="editContact.jsp?id=&locale=en&cancel=true">
												<button type="button" class="btn btn-default">Cancel</button>
											</a>

										</div>
										<!-- /.box-footer -->
									</form>

								</div>
							</div>

						</div>

					</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>



			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<!-- Main Footer -->
		<footer class="main-footer">
			<!-- To the right -->
			<div class="pull-right hidden-xs">Ariaban.ir</div>
			<!-- Default to the left -->
			<strong>Copyright &copy; 2017 <a href="http://ariaban.ir">Ariaban</a>.
			</strong> All rights reserved.
		</footer>

	</div>
	<!-- ./wrapper -->

	<script src="bower_components/jquery/dist/jquery.min.js"></script>
	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="dist/js/Admin-En.min.js"></script>

	<script src="plugins/iCheck/icheck.min.js"></script>
	<script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-yellow',
          radioClass: 'iradio_square-yellow',
          increaseArea: '0%' // optional
        });
      });
    </script>

</body>
</html>
