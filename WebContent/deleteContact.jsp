<%@page import="ir.ariaban.demo.ContactDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:useBean id="u" class="ir.ariaban.demo.Contact"></jsp:useBean>
<jsp:setProperty property="*" name="u"/>

<%

ContactDao.deleteContact(u);
if("fa".equals(request.getParameter("locale")))
	response.sendRedirect("starter-Fa-details.jsp");
else
	response.sendRedirect("starter-En-details.jsp");

%>
