<%@page import="ir.ariaban.demo.ContactDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="editContact" class="ir.ariaban.demo.Contact"></jsp:useBean>
<jsp:setProperty property="*" name="editContact" />

<%
	boolean edited = ContactDao.updateContact(editContact);

	if (edited)
		request.setAttribute("editContact", null);

	String locale = request.getParameter("locale");
	if ("fa".equals(locale))
		response.sendRedirect("starter-Fa-details.jsp");
	else
		response.sendRedirect("starter-En-details.jsp");
%>

