<%@page import="ir.ariaban.demo.Contact"%>
<%@page import="ir.ariaban.demo.ContactDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:useBean id="u" class="ir.ariaban.demo.Contact"></jsp:useBean>
<jsp:setProperty property="*" name="u" />

<%
	String locale = request.getParameter("locale");
	String cancel = request.getParameter("cancel");
	if (cancel != null) {
		ContactDao.editContact = null;
	} else {
		Contact editContact = ContactDao.editById(u.getId());
		request.setAttribute("editContact", editContact);
	}

	RequestDispatcher dispatcher;

	if ("fa".equals(locale))
		dispatcher = getServletContext().getRequestDispatcher("/starter-Fa-details.jsp");
	else
		dispatcher = getServletContext().getRequestDispatcher("/starter-En-details.jsp");

	dispatcher.forward(request, response);
%>
